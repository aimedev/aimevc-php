# Elephaime for PHP 🐘

Ready-to-use lightweight MVC framework. For your small PHP website. Inspired by Laravel 9.  

This package can work as a [Composer dependency](https://packagist.org/users/aimedev/) or as a [Git submodule](https://www.devroom.io/2020/03/09/the-git-submodule-cheat-sheet/).  

A script is provided to help you initialize required files.


## Base requirements

+ `php >= 7.4`
+ (optional) `npm (>= 7.11)` with `laravel-mix` for SASS
+ (optional) `crontab` access, for enabling console tasks


## Installation

### With Composer `>=2.4`
```bash
composer require aimedev/elephaime
```

### With Git `>=2.30.2`
```bash
git submodule add -b main https://gitlab.com/aimedev/elephaime.git

#### OR ####

# In your .gitmodules file
[submodule "elephaime"]
	path = elephaime
	url = git@gitlab.com:aimedev/elephaime.git
	branch = main
```
Then update the submodule to get the last version:
```bash
git config status.submodulesummary 1
git submodule update --init --remote
```

## Initializing the project

```bash
# One-time command for a easier access to the brain tool
php vendor/aimedev/elephaime/src/brain copy # With Composer
php elephaime/src/brain copy # With Git

# For copying default files (required)
php brain init

# If you want to use Laravel Mix (SASS / TypeScript / and static images)
npm install
npm run dev

# Or for raw CSS and JavaScript files
php brain assets
```

### Setting up the environment

After the installation, please take a look at the `.env` file to configure your environment.

To enable console tasks, add this line to your cron table (`crontab -e` with Debian):
```bash
* * * * * /path/to/your/project/www/index.php
```

After these steps, you should check out your new `Routes/web.php` file:
```bash
/path/to/your/project/app/Routes/web.php
```


## Structure

If you are learning the MVC pattern, take a look at [this course](https://openclassrooms.com/fr/courses/4670706-adoptez-une-architecture-mvc-en-php).

Elephaime is a standalone package which works by being at the root-level of your project. You will only edit files in the `app` directory, in which you will write PHP classes under the `App\` namespace.


## Usage

Please take a look at the `USAGE.md` file.


## About

In a way, Elephaime is the "unwanted child" of the [Laravel framework](https://laravel.com).  

It is an easy way to use a Model-View-Controller structure with PHP (with a lower-level of complexity than Laravel).  

It comes with built-in routing, localization, databases and security features.

---

Elephaime is provided by [Nolan aimedev](https://gitlab.com/aimedev).  
[🎫 Submit an issue](https://gitlab.com/aimedev/elephaime/-/issues) (French / English)
