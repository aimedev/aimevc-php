<?php

namespace Aimedev\Elephaime\Core;

class Language {

	public $locale = null;
	public $acceptedLanguages = [];
	private $pieces = [];

	public function __construct() {}

	/**
	 * Check if the localization module is enabled
	 * @param bool $throwError		Throw an error is disabled
	 */
	public function isAvailable(bool $throwError = false) {
		if (env('MULTI_LANG') != true) {
			if ($throwError)
				ErrorHandler::throw(500, 'Env MULTI_LANG variable is not enabled');
			return false;
		}
		return true;
	}

	/**
	 * Set all accepted languages from the browser
	 * @param array $acceptedLanguages
	 */
	public function setAcceptedLanguages(array $acceptedLanguages) {
		$this->isAvailable(true);
		$this->acceptedLanguages = $acceptedLanguages;
	}

	/**
	 * Set current app locale for the user
	 * @param string $locale
	 */
	public function setLocale(string $locale) {
		$this->isAvailable(true);
		if (!in_array($locale, $this->acceptedLanguages))
			ErrorHandler::throw(500, 'Specified locale `' . $locale . '` not defined');

		$file = Storage::getAppAssetsDirectory() . '/lang/' . $locale . '.php';
		if (file_exists($file)) {
			require_once $file;
			$this->locale = $locale;
			$this->pieces = $L;

		} else {
			ErrorHandler::throw(500, 'Specified locale file is not available');
		}
	}

	/**
	 * Get a key part value
	 * @param string $key
	 */
	public function val(string $key) {
		if (!empty($this->pieces[$key]))
			return $this->pieces[$key];
		return '!' . $key;
	}
}
