<?php

namespace Aimedev\Elephaime\Core\Console;

use Aimedev\Elephaime\Core\ErrorHandler;
use Aimedev\Elephaime\Core\Routing\RouteRegistrar;

class Command {

	public $name = '';
	public $closure = null;

	public function __construct(string $name = '', $closure) {
		$this->name = $name;
		$this->closure = $closure;
	}

	public function execute() {
		$mainClosureData = RouteRegistrar::formatClosureForRoute($this->closure);
		if ($mainClosureData['file'] !== null) {
			$closureFile = $mainClosureData['file'];
			$closureFile->{$mainClosureData['fileFunction']}();
		} else if ($mainClosureData['type'] === 'anonymous') {
			call_user_func(RouteRegistrar::getMethodFromClosureData($mainClosureData));
		} else {
			ErrorHandler::throw(500, 'The function registered for the `' . $this->name . '` command is not valid');
		}
	}
}
