<?php

namespace Aimedev\Elephaime\Core\Console;

class Task {

	public $timer = '';
	public $closure = null;

	public function __construct(string $timer, \Closure $closure) {
		$this->timer = $timer;
		$this->closure = $closure;
	}

	public function execute() {
		call_user_func($this->closure);
	}
}
