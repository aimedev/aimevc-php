<?php

namespace Aimedev\Elephaime\Core\Render;

class Page {

	// Access these properties with page()->propertyName

	// Set the page title
	public $title = '';

	// Set the page encoding charset
	public $charset = 'utf-8';

	// Push a value for having it as a CSS class in the <body> tag
	public $bodyClasses = [];

	// Push a key => value for having it as an attribute key="value" in the <body> tag
	public $bodyAttributes = [];

	public function __construct() {}

	// Render a view file
	public function render(string $fileName, array $variables = [], array $excludeVarsForEscaping = []) {
		PageRender::displayView($fileName, $variables, $excludeVarsForEscaping);
	}
}
