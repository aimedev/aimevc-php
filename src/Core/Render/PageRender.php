<?php

namespace Aimedev\Elephaime\Core\Render;

use Aimedev\Elephaime\Core\ErrorHandler;
use Aimedev\Elephaime\Core\Storage;

class PageRender {

	/**
	 * Escape special chars in view
	 * @param array $vars
	 * @param array $exclude 		Variables to exclude
	 */
	private static function escapeVars(array $vars = [], array $exclude = []) {
		foreach ($vars as $key => $value) {
			if (in_array($key, $exclude))
				continue;
			if (!is_string($value))
				continue;
			$vars[$key] = htmlspecialchars($value);
		}
		return $vars;
	}

	/**
	 * Fill file includes
	 * @param string $html
	 * @param array $variables
	 */
	public static function fillIncludes(string $html = '', array $variables = []) {
		$needle = "@include ";
		$lastPos = 0;
		$positions = array();

		while (($lastPos = strpos($html, $needle, $lastPos))!== false) {
			$positions[] = $lastPos;
			$lastPos = $lastPos + strlen($needle);
		}
		$positions = array_reverse($positions);

		foreach ($positions as $pos) {
			$includeName = substr($html, $pos + strlen($needle));
			$includeName = substr($includeName, 0, strpos($includeName, '@'));
			$includeFile = self::getViewPath($includeName);
			if (file_exists($includeFile)) {
				$viewFile = self::getView($includeFile, $variables);
				$html = preg_replace('/\@include ' . str_replace('/', '\/', $includeName) . '\@/', $viewFile, $html);
			}
		}
		return $html;
	}

	/**
	 * Fill language variables in a file
	 * @param string $html
	 */
	public static function fillLanguage(string $html) {
		$needle = "%";
		$lastPos = 0;
		$positions = array();

		while (($lastPos = strpos($html, $needle, $lastPos)) !== false) {
			$positions[] = $lastPos;
			$lastPos = $lastPos + strlen($needle);
		}
		$positions = array_reverse($positions);

		foreach ($positions as $pos) {
			$langVariable = substr($html, $pos);
			preg_match('/^%([a-zA-Z0-9\-_]*)%/', $langVariable, $matches);
			if (!empty($matches[1]))
				$langVariable = $matches[1];
			else
				continue;
			$html = substr_replace($html, lang()->val($langVariable), $pos, strlen($langVariable) + 2);
		}
		return $html;
	}

	/**
	 * Minify HTML for production
	 * @param string $html
	 */
	public static function minifyHTML(string $html) {
		$search = ['/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s', '/<!--(.|\s)*?-->/'];
		$replace = ['>', '<', '\\1', ''];
		$html = preg_replace($search, $replace, $html);
		return $html;
	}

	/**
	 * Make an html page from parts
	 * @param string $mainContent = ''
	 * @param array $parts[$head, $bodyBegin, $bodyEnd, $foot]
	 * @param array $variables
	 */
	public static function buildHTML(string $mainContent, array $parts = [], array $variables = []) {
		// Set default values
		$head = !empty($parts['head']) ? $parts['head'] : '';
		$bodyBegin = !empty($parts['bodyBegin']) ? $parts['bodyBegin'] : '';
		$bodyEnd = !empty($parts['bodyEnd']) ? $parts['bodyEnd'] : '';
		$foot = !empty($parts['foot']) ? $parts['foot'] : '';
		$responsive = is_int($variables['responsive']) ? $variables['responsive'] : true;
		$title = (is_string(page()->title) && !empty(page()->title)
			? (string) page()->title
			: env('APP_NAME'));
		$bodyClasses = implode(' ', page()->bodyClasses);
		if (!empty($bodyClasses))
			$bodyClasses = "class=\"$bodyClasses\"";
		$bodyAttributes = '';
		foreach (page()->bodyAttributes as $attributeKey => $attributeValue) {
			$bodyAttributes .= "$attributeKey=\"$attributeValue\"";
		}

		// Make a big array of variables
		$variables['head'] = $head;
		$variables['bodyBegin'] = $bodyBegin;
		$variables['mainContent'] = $mainContent;
		$variables['bodyEnd'] = $bodyEnd;
		$variables['foot'] = $foot;
		$variables['responsive'] = $responsive;
		$variables['title'] = $title;
		$variables['bodyClasses'] = $bodyClasses;
		$variables['bodyAttributes'] = $bodyAttributes;

		// Get page and fill it
		$basePage = ELEPHAIME_INIT_PATH . '/page.php';
		extract($variables);
		ob_start();
		require_once $basePage;
		$output = ob_get_contents();
		$output = self::fillIncludes($output, $variables);
		ob_end_clean();

		// Return final page
		return $output;
	}

	/**
	 * Display a view file as a page
	 * @param string $fileName
	 * @param array $variables
	 * @param array $excludeVarsForEscaping
	 */
	public static function displayView(string $fileName, array $variables = [], array $excludeVarsForEscaping = []) {
		$viewPath = self::getViewPath($fileName);
		$fileDir = Storage::getCacheDirectory() . '/' . session_id() . '/';

		if (file_exists($viewPath)) {
			$currentRoute = routes()->findRouteByPath(routes()->getRequestPath());

			$controller = (object) [];
			if (!empty($currentRoute->closureFile)) {
				$controller = $currentRoute->closureFile;
			}

			$extraVariables = [
				'controller' => $controller,
				'responsive' => (!empty($controller) && $controller->responsive === true),
				'title' => (!empty($controller) && !empty($controller->title) ? $controller->title : null)
			];

			// Main file
			$variables = self::escapeVars($variables, $excludeVarsForEscaping);
			$mainContent = self::getView($viewPath, array_merge($variables, $extraVariables));

			$htmlParts = [
				// Page content before </head> and just after <body>
				'head' => PageRender::getView('common.head', $extraVariables, false),
				'bodyBegin' => PageRender::getView('common.body-begin', $extraVariables, false),
				// Page content before </body> and just after </body>
				'bodyEnd' => PageRender::getView('common.body-end', $extraVariables, false),
				'foot' => PageRender::getView('common.foot', $extraVariables, false)
			];

			$html = PageRender::buildHTML($mainContent, $htmlParts, $extraVariables);

			if (env('DISABLE_HTML_MINIFY') == false)
				$html = self::minifyHTML($html);

			$cacheFile = $fileDir . '/' . str_replace('/', '__', $fileName) . '.php';
			file_put_contents($cacheFile, $html);
			include $cacheFile;

		} else {
			ErrorHandler::throw(500, 'Unable to render page: undefined view file');
		}
	}

	/**
	 * Get a view content from its name
	 * @param string $name
	 * @param array $variables
	 */
	public static function getView(string $name, array $variables = [], bool $errorIfMissing = true) {
		$filePath = is_file($name) ? $name : self::getViewPath($name);
		$output = null;

		if (file_exists($filePath)) {
			extract($variables);
			ob_start();
			include $filePath;
			$output = ob_get_contents();
			$output = self::fillIncludes($output, $variables);
			ob_end_clean();
			if (lang()->isAvailable())
				$output = self::fillLanguage($output);

		} else if (!empty($errorIfMissing)) {
			$output = ErrorHandler::throw(404, 'Not Found', []);
		}

		return $output;
	}

	/**
	 * Get view path from its name
	 * @param string $viewName
	 */
	public static function getViewPath(string $viewName) {
		return eleph()->systemPaths->views . str_replace('.', '/', $viewName) . '.php';
	}
}
