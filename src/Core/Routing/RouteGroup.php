<?php

namespace Aimedev\Elephaime\Core\Routing;

class RouteGroup {

	public $routesIDs = [];

	public function __construct() {}

	public function addRoute(string $routeIdentifier): self {
		$this->routesIDs[] = $routeIdentifier;
		return $this;
	}

	/**
	 * Set the route name
	 * @param string $routeName
	 */
	public function name(string $routeName) {
		foreach ($this->routesIDs as $routeIdentifier) {
			routes()->getAvailableRoutes()[$routeIdentifier]->customName = $routeName;
		}
		return $this;
	}

	/**
	 * Add a before closure executed before the route main closure
	 * @param string|Closure $stringOrClosure
	 */
	public function before($stringOrClosure) {
		foreach ($this->routesIDs as $routeIdentifier) {
			routes()->getAvailableRoutes()[$routeIdentifier]->before($stringOrClosure);
		}
		return $this;
	}

	/**
	 * Add an after closure executed after the route main closure
	 * @param string|Closure $stringOrClosure
	 */
	public function after($stringOrClosure) {
		foreach ($this->routesIDs as $routeIdentifier) {
			routes()->getAvailableRoutes()[$routeIdentifier]->after($stringOrClosure);
		}
		return $this;
	}
}