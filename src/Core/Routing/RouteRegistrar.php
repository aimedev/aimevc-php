<?php

namespace Aimedev\Elephaime\Core\Routing;

use Aimedev\Elephaime\Core\ErrorHandler;

class RouteRegistrar {

	private $availableRoutes = [];
	private $lastRouteIdentifier = -1;

	/** Force adding before closures to all routes when this variable is not empty */
	private $forceAddBefore = [];

	public function __construct() {}

	/**
	 * Get route path from this current URL request
	 */
	public function getRequestPath(): string {
		$currentPath = str_replace('?' . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
		$appRoot = env('APP_ROOT');
		if (strlen(str_replace('/', '', $appRoot)) > 0 && strpos($currentPath, $appRoot) === 0) {
			$currentPath = substr($currentPath, strlen($appRoot));
		}
		$currentPath = str_replace('//', '/', $currentPath);
		return !empty($currentPath)
			? $currentPath
			: '/';
	}

	public function getAvailableRoutes(): array {
		return $this->availableRoutes;
	}

	/**
	 * Return current route if registered
	 */
	public function checkCurrentRoute() {
		$currentPath = $this->getRequestPath();
		$route = $this->findRouteByPath($currentPath);
		return $route;
	}

	/**
	 * Return the route object from a specified path
	 * @param string $queryPath
	 * @param bool $checkMethod
	 */
	public function findRouteByPath($queryPath, $checkMethod = true) {
		uasort($this->availableRoutes, function($route) {
			// Wildcarded routes and routes with parameters are less prioritized
			return strpos($route->path, '*') !== false || strpos($route->path, ':') !== false;
		});

		foreach ($this->availableRoutes as $route) {
			if ($checkMethod && $route->method !== 'ANY' && $route->method !== $_SERVER['REQUEST_METHOD'])
				continue;
			$pathExploded = explode('/', $route->path);
			$pathVariables = [];
			$pathVariablesTemp = [];
			foreach ($pathExploded as $part) {
				if (strpos($part, ':') === 0) {
					array_push($pathVariablesTemp, $part);
					$route->path = str_replace($part, '([^/]+)', $route->path);
				}
			}
			$regexPath = str_replace(['/', '*'], ['\\/', '(.+?)'], $route->path);
			preg_match('/^' . $regexPath . '\/?$/U', $queryPath, $matches);

			if (!empty($matches[0])) {
				// A route is found for this path
				if (count($matches) > 1) {
					// It has route parameters
					array_shift($matches);
					for ($i = 0; $i < count($matches); $i++) {
						if (empty($pathVariablesTemp[$i]))
							continue;
						$variableName = substr($pathVariablesTemp[$i], 1);
						$pathVariables[$variableName] = $matches[$i];
					}
				}
				$route->parameters = array_merge($route->parameters, $pathVariables);
				return $route;
			}
		}
		return null;
	}

	/**
	 * Return the route object from a specified path
	 * @param string $queryName
	 * @param bool $checkMethod
	 */
	public function findRouteByName($queryName, $checkMethod = true) {
		foreach ($this->availableRoutes as $queryRoute) {
			if ($queryRoute->customName === $queryName) {
				if (!$checkMethod || $queryRoute->method === 'ANY' || $queryRoute->method === $_SERVER['REQUEST_METHOD'])
					return $queryRoute;
			}
		}
		return null;
	}

	/**
	 * Obtains current route and run it
	 */
	public function answerHTTP() {
		$route = $this->checkCurrentRoute();
		if (!$route)
			return ErrorHandler::throw(404, 'Not Found');

		if (!empty($route->befores)) {
			list($allExecuted, $keptVariables) = self::startSequence($route->befores);
			if (!$allExecuted)
				return false;
		}

		$mainClosureData = self::formatClosureForRoute($route->closure);
		if ($mainClosureData['file'] !== null) {
			$route->{'closureFile'} = $mainClosureData['file'];
			if (!empty($keptVariables)) {
				foreach ($keptVariables as $key => $value) {
					$route->{'closureFile'}->$key = $value;
				}
			}
			call_user_func_array([$route->{'closureFile'}, $mainClosureData['fileFunction']], array_values($route->parameters));
		} else if ($mainClosureData['type'] === 'anonymous') {
			if (!empty($keptVariables)) {
				$route->parameters = array_merge($route->parameters, $keptVariables);
			}
			call_user_func_array(self::getMethodFromClosureData($mainClosureData), array_values($route->parameters));
		} else {
			ErrorHandler::throw(500, 'Internal error');
		}

		if (!empty($route->afters)) {
			self::startSequence($route->afters);
		}
	}

	/**
	 * For befores and afters
	 * @param array $sequence
	 */
	public static function startSequence(array $sequence) {
		global $nextStep;
		$indexOfNextFunction = 0;
		$stepsCalled = 0;
		$keepVariablesForEnd = [];
		$nextStep = function($keepVariables = []) use ($sequence, &$indexOfNextFunction, &$stepsCalled, &$keepVariablesForEnd) {
			global $nextStep;
			$stepsCalled++;
			if (!empty($keepVariables)) {
				$keepVariablesForEnd = array_merge($keepVariablesForEnd, $keepVariables);
			}
			if ($indexOfNextFunction >= count($sequence))
				return [true, $keepVariablesForEnd];
			$stepsCalledCache = $stepsCalled;
			$sequence[$indexOfNextFunction++]($nextStep);
			return [($stepsCalledCache < $stepsCalled), $keepVariablesForEnd];
		};
		return $nextStep();
	}

	public static function getMethodFromClosureData($closureData = null) {
		if ($closureData['type'] !== 'anonymous') {
			// Function from controller or helper
			return \Closure::fromCallable([$closureData['file'], $closureData['fileFunction']]);
		} else {
			// Anonymous function
			return $closureData['closure'];
		}
	}

	public static function formatClosureForRoute($closure) {
		$closureType = 'anonymous';
		$closureFileName = null;
		$closureFile = null;
		$closureFileFunction = null;

		if (is_string($closure)) {
			$closure = trim($closure);
			if (preg_match('/([>\.])/', $closure, $matches, PREG_OFFSET_CAPTURE)) {
				$arrowPosition = $matches[1][1];
				$closureFileFunction = substr($closure, $arrowPosition + 1) ?? 'handle';
				$closureFileName = str_replace('/', '\\', substr($closure, 0, $arrowPosition));
				$closureType = (strpos($closureFileName, 'Helper') === strlen($closureFileName) - 6) ? 'Helpers' : 'Controllers';
				$closureFilePath = "App\\" . $closureType . "\\" . $closureFileName;
				$closureFile = new $closureFilePath();
			} else {
				ErrorHandler::throw(500, 'Missing arrow (>) or dot (.) in closure name for a route: ' . $closure);
			}
		}

		return [
			'closure' => $closure,
			'type' => $closureType,
			'file' => $closureFile,
			'fileName' => $closureFileName,
			'fileFunction' => $closureFileFunction
		];
	}

	/**
	 * Register a route with a specific method and callback
	 */
	private function registerRoute($method, $path, $callback) {
		if (!is_array($path))
			$path = [$path];
		if (!in_array($method, ['ANY', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS']))
			ErrorHandler::throw(500, 'Unknown HTTP method: ' . $method);
		if (empty($path) || empty($callback))
			ErrorHandler::throw(500, 'Cannot register a ' . $method . ' route: ' . implode(', ', $path));
		$routeGroup = new RouteGroup();
		foreach ($path as $element) {
			$this->lastRouteIdentifier++;
			$route = new Route($this->lastRouteIdentifier, $method, $element, $callback);
			if (!empty($this->forceAddBefore))
				array_push($route->befores, ...$this->forceAddBefore);
			$routeGroup->addRoute($this->lastRouteIdentifier);
			$this->availableRoutes[$this->lastRouteIdentifier] = $route;
		}
		return $routeGroup;
	}

	/**
	 * @param string|array $path
	 * @param Closure|string $callback
	 * @return RouteGroup
	 */
	public function get($path = '', $callback = '') {
		return $this->registerRoute('GET', $path, $callback);
	}

	/**
	 * @param string|array $path
	 * @param Closure|string $callback
	 * @return RouteGroup
	 */
	public function post($path = '', $callback = '') {
		return $this->registerRoute('POST', $path, $callback);
	}

	/**
	 * @param string|array $path
	 * @param Closure|string $callback
	 * @return RouteGroup
	 */
	public function put($path = '', $callback = '') {
		return $this->registerRoute('PUT', $path, $callback);
	}

	/**
	 * @param string|array $path
	 * @param Closure|string $callback
	 * @return RouteGroup
	 */
	public function patch($path = '', $callback = '') {
		return $this->registerRoute('PATCH', $path, $callback);
	}

	/**
	 * @param string|array $path
	 * @param Closure|string $callback
	 * @return RouteGroup
	 */
	public function delete($path = '', $callback = '') {
		return $this->registerRoute('DELETE', $path, $callback);
	}

	/**
	 * @param string|array $path
	 * @param Closure|string $callback
	 * @return RouteGroup
	 */
	public function options($path = '', $callback = '') {
		return $this->registerRoute('OPTIONS', $path, $callback);
	}

	/**
	 * @param string|array $path
	 * @param Closure|string $callback
	 * @return RouteGroup
	 */
	public function any($path = '', $callback = '') {
		return $this->registerRoute('ANY', $path, $callback);
	}

	/**
	 * Process a method to check if routes should be defined or not
	 * @param string|\Closure $stringOrClosure		Function to execute before running $nextFunction
	 * @param \Closure $nextFunction				Must contain routes to declare if $stringOrClosure ran with success
	 */
	public function declareIf($stringOrClosure, \Closure $nextFunction) {
		$sequence = [];
		if (!is_array($stringOrClosure))
			$stringOrClosure = [$stringOrClosure];

		foreach ($stringOrClosure as $item) {
			$closureData = self::formatClosureForRoute($item);
			$sequence[] = self::getMethodFromClosureData($closureData);
		}

		$sequence[] = $nextFunction;
		self::startSequence($sequence);
	}

	/**
	 * Add a before method to a whole group of routes
	 * @param string|\Closure $stringOrClosure		Function to add as a before for routes defined in $nextFunction
	 * @param \Closure $nextFunction				Must contain routes to define
	 */
	public function before($stringOrClosure, \Closure $nextFunction) {
		if (!is_array($stringOrClosure))
			$stringOrClosure = [$stringOrClosure];

		$this->forceAddBefore = [];
		foreach ($stringOrClosure as $item) {
			$closureData = self::formatClosureForRoute($item);
			$this->forceAddBefore[] = self::getMethodFromClosureData($closureData);
		}

		$nextFunction();
		$this->forceAddBefore = [];
	}
}
