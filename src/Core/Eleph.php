<?php

namespace Aimedev\Elephaime\Core;

class Eleph {

	private static $_instance = null;

	public $systemPaths;
	public $legs;
	public $env;

	private function __construct() {}

	/**
	 * Initialize the Eleph instance
	 */
	public static function self() {
		global $systemPaths;
		if (is_null(self::$_instance)) {
			self::$_instance = new static();
			self::$_instance->systemPaths = $systemPaths;
		}
		return self::$_instance;
	}

	/**
	 * Check if the HTTPS protocol is enabled
	 */
	public static function isHTTPS() {
		if (isset($_SERVER['HTTPS'])) {
			if (strtolower($_SERVER['HTTPS']) == 'on')
				return true;
			if ($_SERVER['HTTPS'] == '1')
				return true;
		} else if (isset($_SERVER['SERVER_PORT'])) {
			$sslPort = 443;
			if ($_SERVER['SERVER_PORT'] == strval($sslPort))
				return true;
		}
		return false;
	}

	/**
	 * Secure usage of cookies
	 * HTTP only, HTTPs only
	 */
	public static function secureCookies() {
		if (function_exists('ini_set') && self::isHTTPS()) {
			ini_set('session.cookie_httponly', '1');
			ini_set("session.cookie_secure", '1');
			ini_set('session.use_only_cookies', '1');
		}
	}

	/**
	 * Prevent this site from being iframed by others
	 */
	public static function secureCSP() {
		header("Content-Security-Policy: frame-ancestors 'self'");
	}
}
