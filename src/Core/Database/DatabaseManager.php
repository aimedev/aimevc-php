<?php

namespace Aimedev\Elephaime\Core\Database;

use Aimedev\Elephaime\Core\ErrorHandler;
use PDO;

class DatabaseManager {

	private $_instances = [];

	public function __construct() {}

	public function instance(string $dbname = ''): DatabaseConnection {
		if (empty($dbname))
			$dbname = 'default';
		if (empty($this->_instances[$dbname])) {
			try {
				$config = $this->getConfig($dbname);
				$dsn = $config['engine'].':dbname='.$config['name'].';host='.$config['host'].';charset=utf8mb4';
				$pdoInstance = new PDO($dsn, $config['user'], $config['password']);
				$this->_instances[$dbname] = new DatabaseConnection($pdoInstance);
			} catch (\PDOException $e) {
				ErrorHandler::throw(500, "Database connection failed: " . $e->getMessage());
			}
		}
		return $this->_instances[$dbname];
	}

	public function getConfig(string $dbname): array {
		$dbsuffix = ($dbname !== 'default' ? "_".strtoupper($dbname) : '');
		$dbconfig = [
			'engine' => env('DATABASE_ENGINE' . $dbsuffix),
			'host' => env('DATABASE_HOST' . $dbsuffix),
			'name' => env('DATABASE_NAME' . $dbsuffix),
			'user' => env('DATABASE_USER' . $dbsuffix),
			'password' => env('DATABASE_PASSWORD' . $dbsuffix)
		];
		foreach ($dbconfig as $key => $value) {
			if ($value === null)
				ErrorHandler::throw(500, "Database connection failed: missing .env parameter (DATABASE_".strtoupper($key).$dbsuffix.")");
		}
		return $dbconfig;
	}
}