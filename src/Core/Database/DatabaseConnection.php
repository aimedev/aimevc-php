<?php

namespace Aimedev\Elephaime\Core\Database;

use Aimedev\Elephaime\Core\ErrorHandler;
use \PDO;

class DatabaseConnection {

	protected static $pdo = null;

	public function __construct(PDO $pdoInstance) {
		self::$pdo = $pdoInstance;
		if (env('APP_DEBUG') == true) {
			self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
	}

	/**
	 * Returns the current PDO instance
	 * @return PDO
	 */
	public static function getConnection(): PDO {
		return self::$pdo;
	}

	/**
	 * Run a database query with PDO
	 * @param string $sql
	 * @param array $bind 	Optional parameters
	 */
	public static function run(string $sql, array $bind = []) {
		$sql = trim($sql);

		try {
			$pdostmt = self::$pdo->prepare($sql);
			if ($pdostmt->execute($bind) !== false) {
				if (preg_match("/^(" . implode("|", array("select", "describe", "pragma")) . ")/i", $sql))
					return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
				else if (preg_match("/^(" . implode("|", array("delete", "insert", "replace", "update")) . ")/i", $sql))
					return $pdostmt->rowCount();
			} else {
				return null;
			}
		} catch (\PDOException $e) {
			return ErrorHandler::errorLog($e);
		}
	}

	/**
	 * @return bool
	 */
	public static function beginTransaction(): bool {
		return self::getConnection()->beginTransaction();
	}

	/**
	 * @return bool
	 */
	public static function validateTransaction(): bool {
		return self::getConnection()->inTransaction()
			? self::getConnection()->commit()
			: false;
	}

	/**
	 * @return bool
	 */
	public static function cancelTransaction(): bool {
		return self::getConnection()->inTransaction()
			? self::getConnection()->rollBack()
			: false;
	}
}
