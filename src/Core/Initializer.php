<?php

namespace Aimedev\Elephaime\Core;

class Initializer {

	public static $excludedFiles = [
		'.gitkeep'
	];

	/**
	 * Begin your project
	 */
	public static function init() {
		echo 'Running initializer script...' . PHP_EOL;

		echo 'Initializing variables...' . PHP_EOL;
		$defaultFilesDir = ELEPHAIME_INIT_PATH . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR;

		if (!is_dir(PROJECT_PATH)) {
			echo "[ERROR] missing main Project folder";
			die();
		}

		if (!is_dir($defaultFilesDir)) {
			echo "[ERROR] missing Initializer subfolder named Files";
			die();
		}

		$rootDir = 'project' . DIRECTORY_SEPARATOR;
		$appDir = 'app' . DIRECTORY_SEPARATOR;
		$storageDir = 'storage' . DIRECTORY_SEPARATOR;
		$wwwDir = 'www' . DIRECTORY_SEPARATOR;

		self::copyDefaultFilesRecursive($rootDir, self::$excludedFiles);
		self::copyDefaultFilesRecursive($appDir, self::$excludedFiles);
		self::copyDefaultFilesRecursive($storageDir, self::$excludedFiles);
		self::copyDefaultFilesRecursive($wwwDir, self::$excludedFiles);

		Storage::makeWwwLinks();
	}

	/**
	 * Copy default files with a recursive strategy
	 * @param string $path
	 * @param array $excludedFiles
	 */
	public static function copyDefaultFilesRecursive(string $path, array $excludedFiles = []) {
		$defaultFilesDir = ELEPHAIME_INIT_PATH . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR;

		$files = array_merge(
			[''],
			array_diff(scandir($defaultFilesDir . $path), array('..', '.'))
		);
		foreach ($files as $file) {
			if (in_array($file, $excludedFiles))
				continue;
			$defaultFilePath = $defaultFilesDir . $path . $file;
			$newFilePath = PROJECT_PATH . (($path === 'project' . DIRECTORY_SEPARATOR) ? '' : $path) . $file;
			$isDir = is_dir($defaultFilePath);

			if ($isDir) {
				if (empty($file)) {
					echo "Creating '" . $newFilePath . "' directory..." . PHP_EOL;
					if (!is_dir($newFilePath))
						mkdir($newFilePath);
				} else {
					self::copyDefaultFilesRecursive($path . $file . DIRECTORY_SEPARATOR, $excludedFiles);
				}

			} else {
				if (file_exists($newFilePath)) {
					echo "[WARN] Cannot copy '$newFilePath' into your new project folder (already one exists)" . PHP_EOL;
				} else {
					$fileCopied = @copy($defaultFilePath, $newFilePath);
					if ($fileCopied)
						echo "Copied '$newFilePath' into your new project folder!" . PHP_EOL;
					else
						echo "[WARN] Cannot copy '$newFilePath' into your new project folder (unknown error)" . PHP_EOL;
				}
			}
		}
	}
}
