<?php

require_once __DIR__ . '/Initializer/namespaces.php';
require_once __DIR__ . '/Initializer/helpers.php';


// Securize cookies and content secure policy
Aimedev\Elephaime\Core\Eleph::secureCookies();
Aimedev\Elephaime\Core\Eleph::secureCSP();


// Start session storage
session_start();


// Cache storage
Aimedev\Elephaime\Core\Storage::clearCache();
Aimedev\Elephaime\Core\Storage::initializeCacheForSession();


// Error handling
if (env('DISABLE_ERROR_AUTO_HANDLER') != true) {
	Aimedev\Elephaime\Core\ErrorHandler::enable();
}


if (php_sapi_name() === 'cli') { // Console requests

	if (is_dir(storage()::getAppDirectory() . 'Console')) {

		// Console tasks
		$consoleTasks = storage()::getAppDirectory() . 'Console/tasks.php';
		if (file_exists($consoleTasks)) {
			echo "🐘 Checking console tasks..." . PHP_EOL;
			require $consoleTasks;
		}

		// Console routes
		$consoleCommands = storage()::getAppDirectory() . 'Routes/console.php';
		if (file_exists($consoleCommands)) {
			echo "🐘 Checking your input for registered console commands..." . PHP_EOL;
			require $consoleCommands;
		}

		// Answer console stuff
		console()->checkTasks();
		console()->answerCommand();

	} else {
		Aimedev\Elephaime\Core\ErrorHandler::throw(500, 'No console directory found');
	}

} else { // HTTP requests

	// Web routes
	$webRoutes = storage()::getAppDirectory() . 'Routes/web.php';
	if (file_exists($webRoutes)) {
		require $webRoutes;
	}

	// Answer web routes
	routes()->answerHTTP();
}
