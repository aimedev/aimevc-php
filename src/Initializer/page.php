<!DOCTYPE html>
<html lang="<?= lang()->locale ?>">
	<head>
		<meta charset="<?= page()->charset ?>">
		<?php if (!empty($responsive)) { ?>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php } ?>
		<title><?= $title ?></title>
		<?= $head ?>
	</head>
	<body <?= $bodyClasses ?> <?= $bodyAttributes ?>>
		<?= $bodyBegin ?>
		<?= $mainContent ?>
		<?= $bodyEnd ?>
	</body>
	<?= $foot ?>
</html>
