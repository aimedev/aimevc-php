<div class="new-installation">
	<img src="<?= asset('images/elephaime.png') ?>">
	<h1>One day, there was an elephant...</h1>
	<p>
		Thanks for installing <b>Elephaime</b>! My goal was to make a PHP website base from nothing but my need of a lightweight framework. I hope it will help you a lot. xoxo Nolan aimedev
	</p>
	<div class="links">
		<a href="https://gitlab.com/aimedev/elephaime/-/blob/main/USAGE.md" target="_blank">How to..?</a>
		<a target="_blank" href="https://gitlab.com/aimedev/elephaime/-/issues">Report an issue</a>
		<a target="_blank" href="https://gitlab.com/aimedev/">Say hello to the author</a>
	</div>
</div>
