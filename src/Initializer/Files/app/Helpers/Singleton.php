<?php

namespace App\Helpers;


abstract class Singleton {

	private static $_instances = [];

	private function __construct() {}
	private function __clone() {}

	final public static function self() {
		$class = get_called_class();
		if (!isset(self::$_instances[$class])) {
			self::$_instances[$class] = new $class();
		}
		return self::$_instances[$class];
	}
}
