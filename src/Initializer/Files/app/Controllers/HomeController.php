<?php

namespace App\Controllers;

class HomeController extends Controller {

	public $title = 'Greeting Elephaime page!';

	public function greeting() {
		page()->title = $this->title;
		page()->render('greeting');
	}

	/**
	 * This is an example on how to use the after() function
	 */
	public function elephantSound(\Closure $next) {
		echo "🐘 <i>The elephant trumpets.</i>";
		echo "<br>";
		$next();
	}
}
