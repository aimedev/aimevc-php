<?php

namespace App\Controllers;

class Controller {
	/**
	 * Keep in mind to extend this controller in a new file
	 *
	 * e.g. HomeController.php
	 * class HomeController extends Controller { ... }
	 */

	// Meta
	public $title = '';

	// For mobile
	public $responsive = true;

	public function __construct() {

	}
}
