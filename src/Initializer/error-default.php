<title><?= $message ?></title>
<div class="error-page">
	<h1><?= $code ?></h1>
	<p>
		<?= $message ?>
	</p>
</div>

<style>

	@import url('https://fonts.googleapis.com/css2?family=Macondo&display=swap');

	:root {
		--body-background: #9aacb6;
	}

	* {
		box-sizing: border-box;
		scroll-behavior: smooth;
	}

	body {
		margin: 0;
		background-color: var(--body-background);
		font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
		font-size: 16px;
	}

	.error-page {
		width: 100vw;
		height: 100vh;
		display: flex;
		flex-direction: column;
		justify-content: start;
		align-items: center;
		text-align: center;
		padding: 150px 20px;
		overflow: auto;
	}

	.error-page > * {
		margin-left: 25px;
		margin-right: 25px;
		max-width: 800px;
		width: 100%;
	}

	.error-page > h1 {
		font-family: 'Macondo', sans-serif;
	}

</style>
