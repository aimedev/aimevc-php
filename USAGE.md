Elephaime for PHP
# Usage

## HTTP Routes
```php
// Add a 'home' route (in routes/web.php) (3 examples below)
// routes()->get(string urlPath, stringOrFunction, array parametersToPass);

routes()->get('/', 'HomeController.index', []);
routes()->get('/', function() { page()->render('home', []); }, []);
routes()->get('/', function() { echo "Home page - hello world!" }, []);
```
```php
// You can add multiple paths with a group assignement (= with an array)
routes()->get([
	'/',
	'/home',
	'/landing'
], 'HomeController.index', []);
```
```php
// Add a route matching any HTTP method
routes()->any('/messages/:action', 'MessagesController.manage', []);

// Add a route matching any subfolder case
routes()->post('/messages/*', 'MessagesController.post', []);

// Add a route matching specified parameters
routes()->post('/messages/:messageID/manage/:actionName', 'MessagesController.post', []);

// Accessing the populated parameters in the function inside of your controller:
// function post($messageID, $actionName) {
//     // Your code here...
// }
```

### Naming routes

```php
// Set a name for a route
routes()->post('/messages/publisher', 'MessagesController.publisher', [])
	->name('messages.publisher');

// Accessing a named route
$publisherRoute = routes()->findRouteByName('messages.publisher', false); // false to disable HTTP method check
$publisherRoute->go(); // Redirecting to the route
```

### Befores and afters
For linking conditions to your routes.
```php
// Ask a function to be run before your main route function
routes()->post('/messages/publisher', 'MessagesController.publisher', [])
	->name('messages.publisher')
	->before('UserController.hasPublisherRole');

// Ask a function to be run after your main route function
routes()->post('/messages/publisher', 'MessagesController.publisher', [])
	->name('messages.publisher')
	->after('NotificationController.alertFollowers');

// For defining routes only if a condition has been passed
routes()->before('UserController.isAuthentified', function() {
	// Define your routes here
	routes()->get('/', 'UserController.resetPassword', []);
	routes()->get('/', 'UserController.changePassword', []);
	//etc
});

// Example
routes()->before(function($next) {
	$user = new User();
	if ($user->notAuth()) return; // Fail => prevent routes to be declared
	$next(); // Success => resolve the before
}, function() {
	routes()->get('/', 'UserController.resetPassword', []);
	routes()->get('/', 'UserController.changePassword', []);
	// etc
});

// References to controllers can also be transformed to anonymous functions btw

// You can also pass helpers: please name your files like RequestHelper if you want to call RequestHelper.checkStatus() function as a before
```

## Views
```php
// Display a view
page()->render('viewName', [
	'hello' => 'world!'
]);
```

## Views parts
These views are special and will be automatically included in all of your pages:
+ `common/head.php` → for all of your meta tags before `</head>` ;
+ `common/body-begin.php` → for body headers, like a navbar or something like this just after `<body>` ;
+ `common/body-end.php` → for your last HTML content before `</body>` ;
+ `common/foot.php` → for your scripts after `</body>`

## Views commons
By editing these variables, you will impact all of your views:
+ `page()->title` → this is your page title ;
+ `page()->charset` → this is your page charset (encoding) ;
+ `page()->bodyClasses` → push a $value to add it to your body as a CSS `class` ;
+ `page()->bodyAttribues` → push a $key => $value to add it to your body as an HTML attribute `$key="$value"` ;

## Localization

```php
// Use localized variables in views
// %nameOfTheVariable%

<h1>%whichSummerTitle%</h1>
<p>%whichSummerAnswerFrench%</p>

<h1>Which country?</h1>
<p>Aaaaah! French summer is the best one!</p>
```
```php
// To enable this module, you must use this snippet:

lang()->setAcceptedLanguages(['en', 'fr']);
lang()->setLocale('en'); // Current user locale
```

## Database queries (PDO)

```
// Register the database configuration in the .env file:
DATABASE_ENGINE_MYWEBSITE=mysql
DATABASE_HOST_MYWEBSITE=127.0.0.1
DATABASE_NAME_MYWEBSITE=
DATABASE_USER_MYWEBSITE=
DATABASE_PASSWORD_MYWEBSITE=
```

```php
// Make a raw SQL request (without models)
// The database function returns a connection using your DATABASE_*_MYWEBSITE env variables

database('mywebsite')::run("SELECT * FROM table1 WHERE column1 = :val1;", ["val1" => "test"]);
```

## Console tasks

```php
// Define a scheduled task (in app/Console/tasks.php)
// Valid timers are:
//     - every X minute(s)    (X = [0-60] or [even|odd] or *)
//     - every X hour(s)      (X = [0-24] or [even|odd] or *)
//     - every X day(s)       (X = [0-31] or [even|odd] or *)
//     - at XX:YY             (X = [0-24] or **, Y = [0-60] or **)

console()->task('at 00:00', function() {
	print "New day, new problems!";
});

console()->task('at 22:**', function() {
	print "A task executed every minute when it is 10 pm (22h FR)";
});

console()->task('every 10 minutes', function() {
	print "A task executed every 10 minutes";
});
```

## Console commands

```php
// Define a command to use with php brain <commandName>

console()->command('hello', function() {
	print('world!');
});
```
